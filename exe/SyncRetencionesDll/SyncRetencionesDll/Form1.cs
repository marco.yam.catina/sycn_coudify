﻿using SyncRetencionesDll.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SyncRetencionesDll
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        ServiceController service = new ServiceController();
        private void button1_Click(object sender, EventArgs e)
        {
            service.Stop();
            service.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            service.Stop();
        }
    }
}
