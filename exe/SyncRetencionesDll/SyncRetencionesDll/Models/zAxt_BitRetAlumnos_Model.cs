﻿using System;
using System.Data.Linq.Mapping;

namespace SyncRetencionesDll.Models
{
    [Table(Name = "zAxt_BitRetAlumnos")]
    public class zAxt_BitRetAlumnos_Model
    {

        [Column(Name = "ID_BITACORA",IsPrimaryKey =true,IsDbGenerated = true)]
        public int ID_BITACORA { get; set; }

        [Column(Name = "CREATE_DATE")]
        public DateTime CREATE_DATE { get; set; }

        [Column(Name = "CREATE_TIME")]
        public string CREATE_TIME { get; set; }

        [Column(Name = "CREATE_OPID")]
        public string CREATE_OPID { get; set; }

        [Column(Name = "CREATE_TERMINAL")]
        public string CREATE_TERMINAL { get; set; }

        [Column(Name = "REVISION_DATE")]
        public DateTime REVISION_DATE { get; set; }

        [Column(Name = "REVISION_TIME")]
        public string REVISION_TIME { get; set; }

        [Column(Name = "REVISION_OPID")]
        public string REVISION_OPID { get; set; }

        [Column(Name = "REVISION_TERMINAL")]
        public string REVISION_TERMINAL { get; set; }

        [Column(Name = "ESTATUS")]
        public int ESTATUS { get; set; }

        [Column(Name = "MESSAGE")]
        public string MESSAGE { get; set; }

        [Column(Name = "NBACCION")]
        public string NBACCION { get; set; }

        [Column(Name = "statecode")]
        public string statecode { get; set; }

        [Column(Name = "ownerid")]
        public string ownerid { get; set; }

        [Column(Name = "firstname")]
        public string firstname { get; set; }

        [Column(Name = "scolaris_segundonombre")]
        public string scolaris_segundonombre { get; set; }

        [Column(Name = "scolaris_apellidopaterno")]
        public string scolaris_apellidopaterno { get; set; }

        [Column(Name = "scolaris_apellidomaterno")]
        public string scolaris_apellidomaterno { get; set; }

        [Column(Name = "scolaris_nombrelegal")]
        public string scolaris_nombrelegal { get; set; }

        [Column(Name = "gendercode")]
        public string gendercode { get; set; }

        [Column(Name = "scolaris_ciudadania")]
        public string scolaris_ciudadania { get; set; }

        [Column(Name = "scolaris_estadocivil")]
        public string scolaris_estadocivil { get; set; }

        [Column(Name = "birthdate")]
        public DateTime birthdate { get; set; }

        [Column(Name = "scolaris_curp")]
        public string scolaris_curp { get; set; }

        [Column(Name = "scolaris_matricula")]
        public string scolaris_matricula { get; set; }

        [Column(Name = "scolaris_marca")]
        public string scolaris_marca { get; set; }

        [Column(Name = "scolaris_calle")]
        public string scolaris_calle { get; set; }

        [Column(Name = "scolaris_numero")]
        public string scolaris_numero { get; set; }

        [Column(Name = "scolaris_numerointerior")]
        public string scolaris_numerointerior { get; set; }

        [Column(Name = "scolaris_colonia")]
        public string scolaris_colonia { get; set; }

        [Column(Name = "scolaris_codigopostal")]
        public string scolaris_codigopostal { get; set; }

        [Column(Name = "scolaris_delegacionmunicipio")]
        public string scolaris_delegacionmunicipio { get; set; }

        [Column(Name = "scolaris_estado")]
        public string scolaris_estado { get; set; }

        [Column(Name = "scolaris_pais")]
        public string scolaris_pais { get; set; }

        [Column(Name = "scolaris_tipodireccion")]
        public string scolaris_tipodireccion { get; set; }

        [Column(Name = "emailaddress1")]
        public string emailaddress1 { get; set; }

        [Column(Name = "scolairs_correoinstitucional")]
        public string scolairs_correoinstitucional { get; set; }

        [Column(Name = "mobilephone")]
        public string mobilephone { get; set; }

        [Column(Name = "telephone2")]
        public string telephone2 { get; set; }

        [Column(Name = "scolaris_telefonooficina")]
        public string scolaris_telefonooficina { get; set; }

        [Column(Name = "scolaris_nocontactar")]
        public string scolaris_nocontactar { get; set; }

        [Column(Name = "scolaris_razonparanocontactarlo")]
        public string scolaris_razonparanocontactarlo { get; set; }

        [Column(Name = "scolaris_facebook")]
        public string scolaris_facebook { get; set; }

        [Column(Name = "scolaris_instagram")]
        public string scolaris_instagram { get; set; }

        [Column(Name = "scolaris_twitter")]
        public string scolaris_twitter { get; set; }

        [Column(Name = "scolaris_nombredelpadre")]
        public string scolaris_nombredelpadre { get; set; }

        [Column(Name = "scolaris_apellidopaternodelpadre")]
        public string scolaris_apellidopaternodelpadre { get; set; }

        [Column(Name = "scolaris_apellidomaternodelpadre")]
        public string scolaris_apellidomaternodelpadre { get; set; }

        [Column(Name = "scolaris_correoelectronicodelpadre")]
        public string scolaris_correoelectronicodelpadre { get; set; }

        [Column(Name = "scolaris_celulardelpadre")]
        public string scolaris_celulardelpadre { get; set; }

        [Column(Name = "scolaris_telefonodelpadre")]
        public string scolaris_telefonodelpadre { get; set; }

        [Column(Name = "scolaris_nombredelamadre")]
        public string scolaris_nombredelamadre { get; set; }

        [Column(Name = "scolaris_apellidopaternodelamadre")]
        public string scolaris_apellidopaternodelamadre { get; set; }

        [Column(Name = "scolaris_apellidomaternodelamadre")]
        public string scolaris_apellidomaternodelamadre { get; set; }

        [Column(Name = "scolaris_correoelectronicodelamadre")]
        public string scolaris_correoelectronicodelamadre { get; set; }

        [Column(Name = "scolaris_celulardelamadre")]
        public string scolaris_celulardelamadre { get; set; }

        [Column(Name = "scolaris_telefonodelamadre")]
        public string scolaris_telefonodelamadre { get; set; }
    }
}
