﻿using System.Data.Linq.Mapping;

namespace SyncRetencionesDll.Models
{
    [Table(Name = "DEMOGRAPHICS")]
    public class DEMOGRAPHICS_Model
    {

        [Column(Name = "GENDER")]
        public string GENDER { get; set; }


        [Column(Name = "CITIZENSHIP")]
        public string CITIZENSHIP { get; set; }

        [Column(Name = "MARITAL_STATUS")]
        public string MARITAL_STATUS { get; set; }

    }
}
