﻿using System;
using System.Data.Linq.Mapping;

namespace SyncRetencionesDll.Models
{
    [Table(Name = "zAxt_ProcesosCOB")]
    public class zAxt_ProcesosCOB_Model
    {

        [Column(Name = "numFacturaOrigen")]
        public string numFacturaOrigen { get; set; }

        [Column(Name = "numCobro")]
        public string numCobro { get; set; }

        [Column(Name = "numFacturaNueva")]
        public string numFacturaNueva { get; set; }

        [Column(Name = "fgAplicado")]
        public bool fgAplicado { get; set; }

        [Column(Name = "feOperacion")]
        public DateTime feOperacion { get; set; }

        [Column(Name = "dcCantidad")]
        public decimal dcCantidad { get; set; }

        [Column(Name = "fgFacturaContado")]
        public bool fgFacturaContado { get; set; }

        [Column(Name = "numLetraFactura")]
        public string numLetraFactura { get; set; }

        [Column(Name = "tpOperacion")]
        public int tpOperacion { get; set; }

        [Column(Name = "numLetraFacturaNueva")]
        public string numLetraFacturaNueva { get; set; }
    }
}
