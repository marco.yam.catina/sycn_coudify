﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncRetencionesDll.Models
{
    public class Alumno
    {
        /*Datos alumno*/
        public string statecode;
        public string ownerid;
        public string firstname;
        public string scolaris_segundonombre;
        public string scolaris_apellidopaterno;
        public string scolaris_apellidomaterno;
        public string scolaris_nombrelegal;
        public string gendercode;
        public string scolaris_ciudadania;
        public string scolaris_estadocivil;
        public string birthdate;
        public string scolaris_curp;
        //public string scolaris_montodeuda;
        public string scolaris_matricula;


        /*Datos de Domicilio*/
        public string scolaris_calle;
        public string scolaris_numero;
        public string scolaris_numerointerior;
        public string scolaris_colonia;
        public string scolaris_codigopostal;
        public string scolaris_delegacionmunicipio;
        public string scolaris_estado;
        public string scolaris_pais;
        public string scolaris_tipodireccion;
        
        /*Datos de Contacto*/
        public string emailaddress1;
        public string scolairs_correoinstitucional;
        public string mobilephone;
        public string telephone2;
        public string scolaris_telefonooficina;
        public bool scolaris_nocontactar;
        public string scolaris_razonparanocontactarlo;
        public string scolaris_facebook;
        public string scolaris_instagram;
        public string scolaris_twitter;

        /*Datos familiares*/
        public string scolaris_nombredelpadre;
        public string scolaris_apellidopaternodelpadre;
        public string scolaris_apellidomaternodelpadre;
        public string scolaris_correoelectronicodelpadre;
        public string scolaris_celulardelpadre;
        public string scolaris_telefonodelpadre;

        public string scolaris_nombredelamadre;
        public string scolaris_apellidopaternodelamadre;
        public string scolaris_apellidomaternodelamadre;
        public string scolaris_correoelectronicodelamadre;
        public string scolaris_celulardelamadre;
        public string scolaris_telefonodelamadre;
        

        public Alumno()
        {

        }




        
    }
}
