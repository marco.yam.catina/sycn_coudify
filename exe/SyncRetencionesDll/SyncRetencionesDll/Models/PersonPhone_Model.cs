﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncRetencionesDll.Models
{
    [Table(Name = "PersonPhone")]
    public class PersonPhone_Model
    {

        [Column(Name = "PersonPhoneId")]
        public int PersonPhoneId { get; set; }

        [Column(Name = "PersonId")]
        public int PersonId { get; set; }

        [Column(Name = "CountryId")]
        public int CountryId { get; set; }

        [Column(Name = "PhoneNumber")]
        public string PhoneNumber { get; set; }

        [Column(Name = "Description")]
        public string Description { get; set; }

        [Column(Name = "DoNotCallReason")]
        public int DoNotCallReason { get; set; }

        [Column(Name = "Create_Date")]
        public DateTime Create_Date { get; set; }

        [Column(Name = "Create_Time")]
        public DateTime Create_Time { get; set; }

        [Column(Name = "Create_Opid")]
        public string Create_Opid { get; set; }

        [Column(Name = "Create_Terminal")]
        public string Create_Terminal { get; set; }

        [Column(Name = "Revision_Date")]
        public DateTime Revision_Date { get; set; }

        [Column(Name = "Revision_Time")]
        public DateTime Revision_Time { get; set; }

        [Column(Name = "Revision_Opid")]
        public string Revision_Opid { get; set; }

        [Column(Name = "Revision_Terminal")]
        public string Revision_Terminal { get; set; }

        [Column(Name = "PhoneType")]
        public string PhoneType { get; set; }
    }
}
