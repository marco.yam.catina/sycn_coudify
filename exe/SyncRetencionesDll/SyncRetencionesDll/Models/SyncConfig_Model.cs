﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncRetencionesDll.Models
{
    [Table(Name = "SyncConfig")]
    public class SyncConfig_Model
    {

        [Column(Name = "SyncConfigId",IsPrimaryKey =true,IsDbGenerated = true)]
        public int SyncConfigId { get; set; }

        [Column(Name = "TpConfig")]
        public string TpConfig { get; set; }

        [Column(Name = "Interval")]
        public int Interval { get; set; }

        [Column(Name = "CountRegister")]
        public int CountRegister { get; set; }

        [Column(Name = "ActRegister")]
        public int ActRegister { get; set; }

        [Column(Name = "FlagSeconds")]
        public bool FlagSeconds { get; set; }

        [Column(Name = "FlagSync")]
        public bool FlagSync { get; set; }
    }
}
