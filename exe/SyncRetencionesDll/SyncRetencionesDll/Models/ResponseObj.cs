﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDao.Models
{
    public class ResponseObj
    {
        public ResponseObj()
        {
            this.estatus = -1;
            this.mgs = "No fue posible conectar al servidor";
            this.data = null;
        }

        public int estatus { get; set; }
        public string mgs { get; set; }
        public object data { get; set; }
    }
}
