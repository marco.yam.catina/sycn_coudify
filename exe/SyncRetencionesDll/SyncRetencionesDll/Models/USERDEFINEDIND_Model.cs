﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncRetencionesDll.Models
{
    [Table(Name = "USERDEFINEDIND")]
    public class USERDEFINEDIND_Model
    {

        [Column(Name = "CURP")]
        public string CURP { get; set; }
        
        [Column(Name = "N_P")]
        public string N_P { get; set; }

        [Column(Name = "AP_P")]
        public string AP_P { get; set; }

        [Column(Name = "AM_P")]
        public string AM_P { get; set; }

        [Column(Name = "N_M")]
        public string N_M { get; set; }

        [Column(Name = "AM_M")]
        public string AM_M { get; set; }

        [Column(Name = "AP_M")]
        public string AP_M { get; set; }

        [Column(Name = "TEL_P")]
        public string TEL_P { get; set; }

        [Column(Name = "TEL_M")]
        public string TEL_M { get; set; }

        [Column(Name = "EMAIL_P")]
        public string EMAIL_P { get; set; }

        [Column(Name = "EMAIL_M")]
        public string EMAIL_M { get; set; }

        [Column(Name = "CEL_P")]
        public string CEL_P { get; set; }

        [Column(Name = "CEL_M")]
        public string CEL_M { get; set; }


    }
}
