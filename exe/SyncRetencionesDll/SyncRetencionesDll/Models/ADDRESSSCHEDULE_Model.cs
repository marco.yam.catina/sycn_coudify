﻿using System;
using System.Data.Linq.Mapping;

namespace SyncRetencionesDll.Models
{
    [Table(Name = "ADDRESSSCHEDULE")]
    public class ADDRESSSCHEDULE_Model
    {


        [Column(Name = "ADDRESS_LINE_1")]
        public string ADDRESS_LINE_1 { get; set; }

        [Column(Name = "ADDRESS_LINE_2")]
        public string ADDRESS_LINE_2 { get; set; }

        [Column(Name = "ADDRESS_LINE_3")]
        public string ADDRESS_LINE_3 { get; set; }

        [Column(Name = "HOUSE_NUMBER")]
        public string HOUSE_NUMBER { get; set; }

        [Column(Name = "CITY")]
        public string CITY { get; set; }

        [Column(Name = "ZIP_CODE")]
        public string ZIP_CODE { get; set; }

        [Column(Name = "STATE")]
        public string STATE { get; set; }

        [Column(Name = "COUNTRY")]
        public string COUNTRY { get; set; }

        [Column(Name = "ADDRESS_TYPE")]
        public string ADDRESS_TYPE { get; set; }

        [Column(Name = "EMAIL_ADDRESS")]
        public string EMAIL_ADDRESS { get; set; }

    }
}
