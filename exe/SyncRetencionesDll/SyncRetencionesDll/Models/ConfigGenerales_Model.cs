﻿using System.Data.Linq.Mapping;

namespace SyncRetencionesDll.Models
{
    [Table(Name = "ConfigGenerales")]
    public class ConfigGenerales_Model
    {
        [Column(Name = "id", IsPrimaryKey = true, IsDbGenerated = true)]
        public int id { get; set; }

        [Column(Name = "nbModulo")]
        public string nbModulo { get; set; }

        [Column(Name = "fgActivo")]
        public bool fgActivo { get; set; }

        [Column(Name = "invervaloActualizacion")]
        public int invervaloActualizacion { get; set; }

        [Column(Name = "numCantidadInsercion")]
        public int numCantidadInsercion { get; set; }

        [Column(Name = "numCantidadActualizacion")]
        public int numCantidadActualizacion { get; set; }

        [Column(Name = "pathCarpetaIncial")]
        public string pathCarpetaIncial { get; set; }

    }
}
