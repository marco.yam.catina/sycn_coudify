﻿using System;
using System.Data.Linq.Mapping;

namespace SyncRetencionesDll.Models
{
    [Table(Name = "PEOPLE")]
    public class PEOPLE_Model
    {

        [Column(Name = "PEOPLE_CODE")]
        public string PEOPLE_CODE { get; set; }

        [Column(Name = "PEOPLE_ID")]
        public string PEOPLE_ID { get; set; }

        [Column(Name = "PEOPLE_CODE_ID")]
        public string PEOPLE_CODE_ID { get; set; }

        [Column(Name = "PREVIOUS_ID")]
        public string PREVIOUS_ID { get; set; }

        [Column(Name = "GOVERNMENT_ID")]
        public string GOVERNMENT_ID { get; set; }

        [Column(Name = "PREV_GOV_ID")]
        public string PREV_GOV_ID { get; set; }

        [Column(Name = "PREFIX")]
        public string PREFIX { get; set; }

        [Column(Name = "FIRST_NAME")]
        public string FIRST_NAME { get; set; }

        [Column(Name = "MIDDLE_NAME")]
        public string MIDDLE_NAME { get; set; }

        [Column(Name = "LAST_NAME")]
        public string LAST_NAME { get; set; }

        [Column(Name = "SUFFIX")]
        public string SUFFIX { get; set; }

        [Column(Name = "NICKNAME")]
        public string NICKNAME { get; set; }

        [Column(Name = "PREFERRED_ADD")]
        public string PREFERRED_ADD { get; set; }

        [Column(Name = "BIRTH_DATE")]
        public DateTime BIRTH_DATE { get; set; }

        [Column(Name = "BIRTH_CITY")]
        public string BIRTH_CITY { get; set; }

        [Column(Name = "BIRTH_STATE")]
        public string BIRTH_STATE { get; set; }

        [Column(Name = "BIRTH_ZIP_CODE")]
        public string BIRTH_ZIP_CODE { get; set; }

        [Column(Name = "BIRTH_COUNTRY")]
        public string BIRTH_COUNTRY { get; set; }

        [Column(Name = "BIRTH_COUNTY")]
        public string BIRTH_COUNTY { get; set; }

        [Column(Name = "DECEASED_DATE")]
        public DateTime DECEASED_DATE { get; set; }

        [Column(Name = "DECEASED_FLAG")]
        public string DECEASED_FLAG { get; set; }

        [Column(Name = "RELEASE_INFO")]
        public string RELEASE_INFO { get; set; }

        [Column(Name = "CREATE_DATE")]
        public DateTime CREATE_DATE { get; set; }

        [Column(Name = "CREATE_TIME")]
        public DateTime CREATE_TIME { get; set; }

        [Column(Name = "CREATE_OPID")]
        public string CREATE_OPID { get; set; }

        [Column(Name = "CREATE_TERMINAL")]
        public string CREATE_TERMINAL { get; set; }

        [Column(Name = "REVISION_DATE")]
        public DateTime REVISION_DATE { get; set; }

        [Column(Name = "REVISION_TIME")]
        public DateTime REVISION_TIME { get; set; }

        [Column(Name = "REVISION_OPID")]
        public string REVISION_OPID { get; set; }

        [Column(Name = "REVISION_TERMINAL")]
        public string REVISION_TERMINAL { get; set; }

        [Column(Name = "ABT_JOIN")]
        public string ABT_JOIN { get; set; }

        [Column(Name = "TAX_ID")]
        public string TAX_ID { get; set; }

        [Column(Name = "PersonId")]
        public int PersonId { get; set; }

        [Column(Name = "PrimaryPhoneId")]
        public int PrimaryPhoneId { get; set; }

        [Column(Name = "Last_Name_Prefix")]
        public string Last_Name_Prefix { get; set; }

        [Column(Name = "LegalName")]
        public string LegalName { get; set; }
    }
}
