﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using SyncRetencionesDll.Controllers;
using System;
using System.Collections.Generic;
using System.Net;

namespace DemoScolarisAPI.BO
{
    public static class Scolarisapi
    {

        public static string CreateSchemaFormat(int valschema, string obj)
        {
            try
            {
                List<Object> studList = new List<object>();
                JObject schema =
                    new JObject(
                        new JProperty("Esquema", valschema),
                        new JProperty("Campo", JObject.Parse(obj))

                );
                studList.Add(schema);
                string jsonCreate = JsonConvert.SerializeObject(studList);
                return jsonCreate;
            }
            catch (Exception ex)
            {
                LogController.RegisterLog("Scolarisapi:CreateRecord", "CreateRecord", ex.ToString());
                return "";
            }

        }
        public static bool CreateRecord(string jsonbBody, ref string mgs)
        {
            try
            {
                var body = JsonConvert.DeserializeObject(jsonbBody);
                var client = new RestClient("https://azscuapimngscolpr01.azure-api.net/recepcion/api/scolarisdt");
                var request = new RestRequest(Method.POST);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                request.AddHeader("Ocp-Apim-Subscription-Key", "90839d3d570542f98f87564504d228a9");
                request.AddHeader("Content-Type", "application/json");

                request.AddParameter("application/json", body, ParameterType.RequestBody);
                RestResponse response = (RestSharp.RestResponse)client.Execute(request);
                HttpStatusCode statusCode = response.StatusCode;
                int numericStatusCode = (int)statusCode;

                var mgsContent = $"EstatusCode [{numericStatusCode}], Content [{response.Content}]";

                Console.WriteLine(response.Content);
                Console.WriteLine($"finished con status code de {numericStatusCode}");

                LogController.RegisterLog("Scolarisapi:CreateRecord", "CreateRecord", $"{mgs} -> json[{jsonbBody}]");
                LogController.RegisterLog("Scolarisapi:CreateRecord->", "json", jsonbBody);

                if (numericStatusCode == 200)
                {
                    mgs = $"Operación realizada con éxito -> {mgsContent}";
                    return true;
                }
                else
                {
                    mgs = $"No fue posible realizar la operación -> {mgsContent}";
                    return false;
                }



            }
            catch (Exception e)
            {

                mgs = $"No fue posible realizar la operación {e.ToString()}";

                LogController.RegisterLog("Scolarisapi:CreateRecord", "CreateRecord", e.ToString());

                return false;

                //Console.WriteLine(e.ToString());
            }
        }

    }

}
