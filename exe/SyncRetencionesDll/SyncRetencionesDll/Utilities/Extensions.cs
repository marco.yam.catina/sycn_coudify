﻿using SyncRetencionesDll.Controllers;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;


namespace System
{
    public static class Extensions
    {
        #region DataTable
        public static List<T> ConvertDataTable<T>(this DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName && dr[column.ColumnName] != DBNull.Value)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }


        #endregion

        public static void RegisterLog(this object obj, string function, string nivelCatch, string message)
        {
            //Extensions.RegisterLog()
            var fileName = $"{Constants.path_Log}Log_{DateTime.Now.ToString("ddMMyyyy")}.txt";

            try
            {
                if (!Directory.Exists(Constants.path_Log))
                    Directory.CreateDirectory(Constants.path_Log);

                using (StreamWriter writer = new StreamWriter(fileName, true))
                {
                    writer.WriteLine($"--------------------------Start-----------------------------");

                    writer.WriteLine($"function: {function}");
                    writer.WriteLine($"nivelCatch: {nivelCatch}");
                    writer.WriteLine($"Hora: {DateTime.Now.ToString("hh:mm:ss tt")}");
                    writer.WriteLine($"message: {message}");

                    writer.WriteLine($"---------------------------Finish----------------------------\n\n");
                    writer.Close();
                }
            }
            catch (Exception ex)
            { //Error write in log }
            }
        }

     
        //public static void ExportModelToText<T>(this T item) where T : BaseModel
        //{
        //    try
        //    {
        //        Type type = typeof(T);

        //        var valueID = "0";
        //        foreach (var pro in type.GetProperties())
        //        {
        //            if (pro.Name == item.Get_Id_Model())
        //            {
        //                valueID = pro.GetValue(item).ToString();
        //            }
        //        }

        //        var queryBit = $"SELECT * FROM BitacoraTransacciones WHERE idDiscriminador = {valueID} AND nbModulo = '{item.Get_Modulo()}' ORDER BY 1 DESC";

        //        var responseBit = new BitacoraTransaccionesController().SelectAll(queryBit);

        //        BitacoraTransacciones_Model bitacora = null;

        //        if (responseBit.estatus == 1)
        //        {
        //            bitacora = (responseBit.data as DataTable).ConvertDataTable<BitacoraTransacciones_Model>()[0];
        //        }

        //        var status = $"[STATUS]{(bitacora.fgAplicado ? "SUCCESS" : "ERROR")}";
        //        var type_action = $"[TYPE]{bitacora.tipoAccion}";
        //        var message = $"[MESSAGE]{bitacora.desMensaje}";

        //        var fileName = $"{item.GetFileName()}{bitacora.tipoAccion.Substring(0, 2)}-{DateTime.Now.ToString("ddMMyyyy")}";

        //        string path = $@"{Constants.path_txt_export}{fileName}.txt";

        //        if (!Directory.Exists(Constants.path_txt_export))
        //            Directory.CreateDirectory(Constants.path_txt_export);

        //        if (!File.Exists(path))
        //        {
        //            // Create a file to write to.
        //            using (StreamWriter sw = File.CreateText(path))
        //            {
        //                sw.WriteLine(status);
        //                sw.WriteLine(type_action);
        //                sw.WriteLine(message);
        //                sw.WriteLine("");

        //                var info = new object().ExportModelString(item);

        //                foreach (var line in info)
        //                {
        //                    sw.WriteLine(line);
        //                }

        //                sw.Close();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.RegisterLog("ExportModelToText", "Export txt", ex.ToString());
        //    }
        //}

        //public static List<String> ExportModelString<T>(this object model, T item) where T : BaseModel
        //{
        //    List<String> lsResult = new List<string>();

        //    try
        //    {
        //        Type type = typeof(T);
        //        var properties = type.GetProperties();

        //        foreach (PropertyInfo info in properties)
        //        {
        //            var columnValue = "";
        //            columnValue = $"[{info.Name}]@";
        //            lsResult.Add(columnValue);
        //        }

        //        for (int i = 0; i < properties.Length; i++)
        //        {

        //            if (properties[i].GetValue(item) != null)
        //            {
        //                var valueStringColumn = "";

        //                valueStringColumn = properties[i].GetValue(item).ToString();


        //                lsResult[i] = lsResult[i].Replace("@", valueStringColumn);
        //            }
        //            else
        //            {
        //                lsResult[i] = lsResult[i].Replace("@", "");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        lsResult = new List<string>();
        //    }


        //    return lsResult;
        //}


       
         
        //public static string getValuesStringModel<T>(this T item) where T : BaseModel
        //{
        //    Type type = typeof(T);
        //    var properties = type.GetProperties();

        //    var query = "";
        //    var isFirst = true;
        //    for (int i = 0; i < properties.Length; i++)
        //    {

        //        //ignorar los id de la tabla
        //        if (properties[i].Name != item.Get_Id_Model() && properties[i].GetValue(item) != null)
        //        {
        //            var value = properties[i].GetValue(item);

        //            var realValue = $"[{ properties[i].Name}] = ";

        //            if (value is string)
        //            {
        //                realValue += $"'{value.ToString()}'";
        //            }
        //            else if (value is DateTime)
        //            {
        //                var date_Value = (DateTime.Parse(value.ToString()));

        //                realValue += $"'{date_Value.ToString("yyyy-MM-dd HH:mm:sss")}'";
        //            }
        //            else if (value is bool)
        //            {
        //                realValue += $"{(((bool)value) == true ? 1 : 0)}";
        //            }
        //            else
        //            {
        //                realValue += $"{value.ToString()}";
        //            }
        //            query += (!isFirst ? "," : "") + realValue;

        //            isFirst = false;
        //        }
        //    }

        //    return query;
        //}

        public static bool IsNullOrEmpty(this string str)
        {
            return (string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str));
        }
    }
}
