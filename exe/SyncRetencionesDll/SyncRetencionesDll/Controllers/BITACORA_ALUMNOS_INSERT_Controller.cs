﻿using SyncRetencionesDll.Models;
using System;
using TestDao.Controllers;
using TestDao.Models;

namespace SyncRetencionesDll.Controllers
{
    public class BITACORA_ALUMNOS_INSERT_Controller : BaseController<zAxt_BitRetAlumnos_Model>
    {
        public ResponseObj Insert(zAxt_BitRetAlumnos_Model item)
        {
            ResponseObj response = new ResponseObj();
            try
            {
                response = db.Insert((table) =>
                {
                    //var existe = (from x in table where);
                    return item;
                });

                if (response.estatus == 1)
                {
                    //Success
                }
                else
                {
                    LogController.RegisterLog("BITACORA_ALUMNOS_INSERT_Controller:Insert->db.Insert", "db.Insert", response.mgs);
                }
            }
            catch (Exception ex)
            {
                LogController.RegisterLog("BITACORA_ALUMNOS_INSERT_Controller:Insert->db.Insert", "ImportItem", ex.ToString());

                response.estatus = -1;
                response.data = null;
                response.mgs = ex.ToString();
            }

            return response;
        }
    }
}
