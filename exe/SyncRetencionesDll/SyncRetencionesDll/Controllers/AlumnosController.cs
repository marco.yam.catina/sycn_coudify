﻿using DemoScolarisAPI.BO;
using Newtonsoft.Json;
using SyncRetencionesDll.Models;
using System;
using System.Data;
using TestDao.Controllers;

namespace SyncRetencionesDll.Controllers
{
    public class AlumnosController : BaseController<PEOPLE_Model>
    {

        public Alumno getAlumnoDemo()
        {
            Alumno obAlum = new Alumno();

            obAlum.statecode = "Activo";
            obAlum.ownerid = "admin crm 2";
            obAlum.firstname = "Example desde c# numero tres";
            obAlum.scolaris_segundonombre = "demo";
            obAlum.scolaris_apellidopaterno = "dummy";
            obAlum.scolaris_apellidomaterno = "test";
            obAlum.scolaris_nombrelegal = "test example";
            obAlum.gendercode = "Hombre";
            obAlum.scolaris_ciudadania = "México";
            obAlum.scolaris_estadocivil = "Soltero";
            obAlum.birthdate = "1999/08/27";
            obAlum.scolaris_curp = "COTAEXMAPLE123";
            obAlum.scolaris_matricula = "456315875";

            obAlum.scolaris_calle = "calle 62a";
            obAlum.scolaris_numero = "573";
            obAlum.scolaris_numerointerior = "por 78 y 80";
            obAlum.scolaris_colonia = "Centro";
            obAlum.scolaris_codigopostal = "97000";
            obAlum.scolaris_delegacionmunicipio = "sn";
            obAlum.scolaris_estado = "Yucatán";
            obAlum.scolaris_pais = "México";
            obAlum.scolaris_tipodireccion = "Casa";

            obAlum.emailaddress1 = "demotest@gmail.com";
            obAlum.scolairs_correoinstitucional = "demo_instituto@gmail.com";
            obAlum.mobilephone = "85693213";
            obAlum.telephone2 = "85693212";
            obAlum.scolaris_telefonooficina = "85693211";
            obAlum.scolaris_nocontactar = false;
            obAlum.scolaris_razonparanocontactarlo = "No existe";
            obAlum.scolaris_facebook = "demotest_face";
            obAlum.scolaris_instagram = "demotest_insta";
            obAlum.scolaris_twitter = "demotest_twt";

            obAlum.scolaris_nombredelpadre = "TestPat";
            obAlum.scolaris_apellidopaternodelpadre = "TestPat_lastn";
            obAlum.scolaris_apellidomaternodelpadre = "TestPat_lastm";
            obAlum.scolaris_correoelectronicodelpadre = "TestPat@outlook.com";
            obAlum.scolaris_celulardelpadre = "12365489";
            obAlum.scolaris_telefonodelpadre = "98456321";
            obAlum.scolaris_nombredelamadre = "Testmat_name";
            obAlum.scolaris_apellidopaternodelamadre = "Testmat_lastn";
            obAlum.scolaris_apellidomaternodelamadre = "Testmat_mat";
            obAlum.scolaris_correoelectronicodelamadre = "testmat@gmail.com";
            obAlum.scolaris_celulardelamadre = "456321897";
            obAlum.scolaris_telefonodelamadre = "798654321";

            return obAlum;
        }

        public void InsertAlumnos(int numBloque)
        {
            var query = $"SELECT TOP {numBloque}" +
                " *" +
                " FROM" +
                " PEOPLE P" +
                " WHERE" +
                " P.PEOPLE_ID IN(SELECT PT.PEOPLE_ID FROM PEOPLETYPE PT WHERE PT.PEOPLE_TYPE = 'STUD')" +
                " AND" +
                " P.PEOPLE_ID NOT IN(SELECT scolaris_matricula FROM zAxt_BitRetAlumnos WHERE scolaris_matricula = P.PEOPLE_ID AND NBACCION = 'INSERT')";

            var responseAlumnos = this.db.ExecuteQueryStringGet(query);

            var bitacoraCtrl = new BITACORA_ALUMNOS_INSERT_Controller();

            if (responseAlumnos.estatus == 1)
            {
                var lsAlumns = (responseAlumnos.data as DataTable).ConvertDataTable<PEOPLE_Model>();

                foreach (var alumnoBase in lsAlumns)
                {
                    var bitacora = new zAxt_BitRetAlumnos_Model();
                    try
                    {




                        var alumnoSend = new Alumno()
                        {

                            firstname = alumnoBase.FIRST_NAME,
                            scolaris_segundonombre = alumnoBase.MIDDLE_NAME,
                            scolaris_apellidopaterno = alumnoBase.LAST_NAME,
                            scolaris_apellidomaterno = alumnoBase.Last_Name_Prefix,
                            scolaris_nombrelegal = alumnoBase.LegalName,
                            scolaris_matricula = alumnoBase.PEOPLE_ID,
                            birthdate = alumnoBase.BIRTH_DATE.ToString("yyyy/MM/dd"),
                            ownerid = "admin crm 2", //Revisar
                            statecode = "Activo" //Revisar

                        };

                        //Datos personales
                        var queryDemogra = $"SELECT TOP 1 * FROM DEMOGRAPHICS WHERE PEOPLE_ID = '{alumnoBase.PEOPLE_ID}'";

                        var responseDemogra = new DEMOGRAPHICS_ModelController().db.ExecuteQueryStringGet(queryDemogra);

                        if (responseDemogra.estatus == 1)
                        {
                            var objDemogra = (responseDemogra.data as DataTable).ConvertDataTable<DEMOGRAPHICS_Model>()[0];

                            alumnoSend.gendercode = (objDemogra.GENDER ?? "M") == "M" ? "Hombre" : "Mujer";

                            if (objDemogra.MARITAL_STATUS == null)
                            {
                                alumnoSend.scolaris_estadocivil = "Soltero";
                            }
                            else if (objDemogra.MARITAL_STATUS == "CASA")
                            {
                                alumnoSend.scolaris_estadocivil = "Casado";
                            }
                            else if (objDemogra.MARITAL_STATUS == "DIVO")
                            {
                                alumnoSend.scolaris_estadocivil = "Divorciado";
                            }
                            else if (objDemogra.MARITAL_STATUS == "OTRO")
                            {
                                alumnoSend.scolaris_estadocivil = "Otro";
                            }
                            else if (objDemogra.MARITAL_STATUS == "SEPA")
                            {
                                alumnoSend.scolaris_estadocivil = "Separado";
                            }
                            else if (objDemogra.MARITAL_STATUS == "SOLT")
                            {
                                alumnoSend.scolaris_estadocivil = "Soltero";
                            }
                            else if (objDemogra.MARITAL_STATUS == "VIUD")
                            {
                                alumnoSend.scolaris_estadocivil = "Viudo";
                            }
                            else
                            {
                                alumnoSend.scolaris_estadocivil = "Soltero";
                            }

                            //alumnoSend.scolaris_estadocivil = objDemogra.MARITAL_STATUS;
                            alumnoSend.scolaris_ciudadania = objDemogra.CITIZENSHIP;

                        }

                        //Datos domiciliarios
                        var queryAddrs = $"select * from ADDRESSSCHEDULE WHERE PEOPLE_ORG_ID = '{alumnoBase.PEOPLE_ID}' AND PEOPLE_ORG_CODE = 'P'";

                        var responseAddrs = new ADDRESSSCHEDULE_ModelController().db.ExecuteQueryStringGet(queryAddrs);

                        if (responseAddrs.estatus == 1)
                        {
                            var objAddress = (responseAddrs.data as DataTable).ConvertDataTable<ADDRESSSCHEDULE_Model>()[0];

                            alumnoSend.scolaris_calle = objAddress.ADDRESS_LINE_1;
                            alumnoSend.scolaris_numero = objAddress.HOUSE_NUMBER;
                            alumnoSend.scolaris_numerointerior = objAddress.ADDRESS_LINE_2;
                            alumnoSend.scolaris_colonia = objAddress.CITY;
                            alumnoSend.scolaris_codigopostal = objAddress.ZIP_CODE;
                            alumnoSend.scolaris_delegacionmunicipio = objAddress.ADDRESS_LINE_3;
                            //alumnoSend.scolaris_estado = objAddress.STATE;
                            alumnoSend.scolaris_estado = "Ciudad de México"; //Valor temporal-> cambiar

                            alumnoSend.scolaris_pais = objAddress.COUNTRY;

                            if (objAddress.ADDRESS_TYPE == null)
                            {
                                alumnoSend.scolaris_tipodireccion = "Casa";
                            }
                            else if (objAddress.ADDRESS_TYPE == "MADR")
                            {
                                alumnoSend.scolaris_tipodireccion = "Madre";
                            }
                            else if (objAddress.ADDRESS_TYPE == "CASA")
                            {
                                alumnoSend.scolaris_tipodireccion = "Casa";
                            }
                            else if (objAddress.ADDRESS_TYPE == "PADR")
                            {
                                alumnoSend.scolaris_tipodireccion = "Padre";
                            }
                            else if (objAddress.ADDRESS_TYPE == "OTRO")
                            {
                                alumnoSend.scolaris_tipodireccion = "Otro";
                            }
                            else if (objAddress.ADDRESS_TYPE == "TRAB")
                            {
                                alumnoSend.scolaris_tipodireccion = "Trabajo";
                            }
                            else
                            {
                                alumnoSend.scolaris_tipodireccion = "Casa";
                            }

                            //alumnoSend.scolaris_tipodireccion = objAddress.ADDRESS_TYPE;
                            alumnoSend.emailaddress1 = objAddress.EMAIL_ADDRESS;

                        }

                        var queryUndefined = $"select top 1 * from USERDEFINEDIND WHERE PEOPLE_ID = '{alumnoBase.PEOPLE_ID}'";

                        var responseUndefined = new USERDEFINEDIND_ModelController().db.ExecuteQueryStringGet(queryUndefined);

                        if (responseUndefined.estatus == 1)
                        {
                            var objUndefined = (responseUndefined.data as DataTable).ConvertDataTable<USERDEFINEDIND_Model>()[0];

                            //Padre
                            alumnoSend.scolaris_nombredelpadre = objUndefined.N_P;
                            alumnoSend.scolaris_apellidopaternodelpadre = objUndefined.AP_P;
                            alumnoSend.scolaris_apellidomaternodelpadre = objUndefined.AM_P;
                            alumnoSend.scolaris_correoelectronicodelpadre = objUndefined.EMAIL_P;
                            alumnoSend.scolaris_celulardelpadre = objUndefined.CEL_P;
                            alumnoSend.scolaris_telefonodelpadre = objUndefined.TEL_P;

                            //Madre
                            alumnoSend.scolaris_nombredelamadre = objUndefined.N_M;
                            alumnoSend.scolaris_apellidopaternodelamadre = objUndefined.AP_M;
                            alumnoSend.scolaris_apellidomaternodelamadre = objUndefined.AM_M;
                            alumnoSend.scolaris_correoelectronicodelamadre = objUndefined.EMAIL_M;
                            alumnoSend.scolaris_celulardelamadre = objUndefined.CEL_M;
                            alumnoSend.scolaris_telefonodelamadre = objUndefined.TEL_M;


                        }

                        var queryPersonPhone = $"select * from PersonPhone where PersonId = {alumnoBase.PersonId}";

                        var responsePersonPhone = new PersonPhone_ModelController().db.ExecuteQueryStringGet(queryPersonPhone);

                        if (responsePersonPhone.estatus == 1)
                        {
                            var lsPersonPhone = (responsePersonPhone.data as DataTable).ConvertDataTable<PersonPhone_Model>();

                            foreach (var objPersonPhone in lsPersonPhone)
                            {
                                if (objPersonPhone.PhoneType == "CASA")
                                {
                                    alumnoSend.telephone2 = objPersonPhone.PhoneNumber;
                                }
                                else if (objPersonPhone.PhoneType == "CELULAR")
                                {
                                    alumnoSend.mobilephone = objPersonPhone.PhoneNumber;
                                }
                                else if (objPersonPhone.PhoneType == "OFICINA")
                                {

                                    alumnoSend.scolaris_telefonooficina = objPersonPhone.PhoneNumber;
                                }

                            }

                            alumnoSend.scolaris_nocontactar = false; //Validar tipo
                            alumnoSend.scolaris_razonparanocontactarlo = lsPersonPhone[0].DoNotCallReason == 0 ? "No existe" : lsPersonPhone[0].DoNotCallReason.ToString(); //Validar tipo

                        }

                        //No definido
                        alumnoSend.scolaris_facebook = "";
                        alumnoSend.scolaris_twitter = "";
                        alumnoSend.scolaris_instagram = "";

                        bitacora = new zAxt_BitRetAlumnos_Model()
                        {
                            ESTATUS = 400,
                            MESSAGE = "NO FUE POSIBLE REALIZAR LA OPERACIÓN",
                            NBACCION = "INSERT",
                            CREATE_DATE = DateTime.Now, //Revisar este campo
                            CREATE_TIME = DateTime.Now.ToString("hh:mm:ss"),
                            REVISION_TIME = DateTime.Now.ToString("hh:mm:ss"),
                            REVISION_TERMINAL = "",
                            REVISION_OPID = "",
                            CREATE_TERMINAL = "",
                            CREATE_OPID = "",
                            REVISION_DATE = DateTime.Now, //Revisar este campo                      
                            scolaris_instagram = alumnoSend.scolaris_instagram,
                            scolaris_twitter = alumnoSend.scolaris_twitter,
                            scolaris_facebook = alumnoSend.scolaris_facebook,
                            birthdate = DateTime.Now, //Validar este campo
                            emailaddress1 = alumnoSend.emailaddress1,
                            firstname = alumnoSend.firstname,
                            gendercode = alumnoSend.gendercode,
                            mobilephone = alumnoSend.mobilephone,
                            ownerid = "admin crm 2", //Revisar
                            scolairs_correoinstitucional = alumnoSend.scolairs_correoinstitucional,
                            scolaris_apellidomaterno = alumnoSend.scolaris_apellidomaterno,
                            scolaris_apellidomaternodelamadre = alumnoSend.scolaris_apellidomaternodelamadre,
                            scolaris_apellidomaternodelpadre = alumnoSend.scolaris_apellidomaternodelpadre,
                            scolaris_apellidopaterno = alumnoSend.scolaris_apellidopaterno,
                            scolaris_apellidopaternodelamadre = alumnoSend.scolaris_apellidopaternodelamadre,
                            scolaris_apellidopaternodelpadre = alumnoSend.scolaris_apellidopaternodelpadre,
                            scolaris_calle = alumnoSend.scolaris_calle,
                            scolaris_celulardelamadre = alumnoSend.scolaris_celulardelamadre,
                            scolaris_celulardelpadre = alumnoSend.scolaris_celulardelpadre,
                            scolaris_ciudadania = alumnoSend.scolaris_ciudadania,
                            scolaris_codigopostal = alumnoSend.scolaris_codigopostal,
                            scolaris_colonia = alumnoSend.scolaris_colonia,
                            scolaris_correoelectronicodelamadre = alumnoSend.scolaris_correoelectronicodelamadre,
                            scolaris_correoelectronicodelpadre = alumnoSend.scolaris_correoelectronicodelpadre,
                            scolaris_curp = alumnoSend.scolaris_curp,
                            scolaris_delegacionmunicipio = alumnoSend.scolaris_delegacionmunicipio,
                            scolaris_estado = alumnoSend.scolaris_estado,
                            scolaris_estadocivil = alumnoSend.scolaris_estadocivil,
                            scolaris_marca = alumnoSend.scolaris_estadocivil,
                            scolaris_matricula = alumnoSend.scolaris_matricula,
                            scolaris_nocontactar = "false", //Validar esta info
                            scolaris_nombredelamadre = alumnoSend.scolaris_nombredelamadre,
                            scolaris_nombredelpadre = alumnoSend.scolaris_nombredelpadre,
                            scolaris_nombrelegal = alumnoSend.scolaris_nombrelegal,
                            scolaris_numero = alumnoSend.scolaris_numero,
                            scolaris_numerointerior = alumnoSend.scolaris_numerointerior,
                            scolaris_pais = alumnoSend.scolaris_pais,
                            scolaris_razonparanocontactarlo = alumnoSend.scolaris_razonparanocontactarlo,
                            scolaris_segundonombre = alumnoSend.scolaris_segundonombre,
                            scolaris_telefonodelamadre = alumnoSend.scolaris_telefonodelamadre,
                            scolaris_telefonodelpadre = alumnoSend.scolaris_telefonodelpadre,
                            scolaris_telefonooficina = alumnoSend.scolaris_telefonooficina,
                            scolaris_tipodireccion = alumnoSend.scolaris_tipodireccion,
                            statecode = "Activo", //Revisar
                            telephone2 = alumnoSend.telephone2

                        };


                        //Enviar al ws
                        string jsonout = JsonConvert.SerializeObject(alumnoSend);
                        string jsonToApi = Scolarisapi.CreateSchemaFormat(3, jsonout);

                        var mgs = "";
                        var success = Scolarisapi.CreateRecord(jsonToApi, ref mgs);

                        bitacora.ESTATUS = success ? 200 : 400;
                        bitacora.MESSAGE = mgs;


                    }
                    catch (Exception ex)
                    {
                        LogController.RegisterLog("AlumnosController:InsertAlumnos", "InsertAlumnos", ex.ToString());

                        bitacora.ESTATUS = 400;
                        bitacora.MESSAGE = $"No fue posible realizar la operación {ex.ToString()}";
                    }

                    var resInsertBit = bitacoraCtrl.Insert(bitacora);

                    if (resInsertBit.estatus != 1)
                    {
                        LogController.RegisterLog("AlumnosController:InsertAlumnos", "InsertAlumnos->Bit", $"estatus: {resInsertBit.estatus}- Mensaje {resInsertBit.mgs}");
                    }

                }
            }
            else
            {
                LogController.RegisterLog("AlumnosController:InsertAlumnos", "InsertAlumnos", $"estatus: {responseAlumnos.estatus}- Mensaje {responseAlumnos.mgs}");
            }
        }
    }
}
