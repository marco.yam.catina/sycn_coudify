﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using TestDao.Models;

namespace SyncRetencionesDll.Controllers
{
    public class DaoContext<T> : DataContext where T : class
    {
        string database, User, server, pwdPassword, connectionString;

        //DataContext db;
        public DaoContext() : base(ConfigureConnection())
        {

        }

        string siteName = "HMM";

        public ResponseObj UpdateOrInsert(string query, params object[] paramssp)
        {
            ResponseObj response = new ResponseObj();
            try
            {

                var formart = String.Format(query, paramssp);

                response.mgs = "Operación realizada con éxito";
                response.estatus = 1;
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error execture query: {ex.ToString()}");
                response.estatus = -1;
                response.mgs = ex.Message;
                response.data = null;

            }

            return response;
        }

        public Table<T> TableGeneric<T>() where T : class
        {
            return this.GetTable<T>();
        }
        public ResponseObj SelectAll()
        {
            ResponseObj response = new ResponseObj();

            try
            {
                var obj = TableGeneric<T>();

                var query = (from x in obj select x).ToList();

                if (query != null && query.Count > 0)
                {
                    response.estatus = 1;
                    response.data = query;
                    response.mgs = "Operación realizada con éxito";
                }
                else
                {
                    response.estatus = 0;
                    response.data = null;
                    response.mgs = "No se encontraron elementos";
                }
            }
            catch (Exception ex)
            {
                response.estatus = -1;
                response.data = null;
                response.mgs = ex.Message;
            }
            return response;
        }

        public ResponseObj ExecuteQueryStringGet(string query, CommandType type = CommandType.Text, params SqlParameter[] parameters)
        {
            ResponseObj response = new ResponseObj();
            try
            {

                //LogController.RegisterLog("connection", "connection", ConfigureConnection());
                //LogController.RegisterLog("connection2", "connection2", this.Connection.ConnectionString);

                SqlConnection sqlConnection = new SqlConnection(ConfigureConnection());
                sqlConnection.Open();



                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                //Tipo de consulta
                sqlCommand.CommandType = type;
                //Parametros opcionales
                if (parameters.Length > 0)
                    sqlCommand.Parameters.AddRange(parameters);

                DataTable table = new DataTable();

                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                adapter.Fill(table);
                sqlConnection.Close();

                adapter.Dispose();

                response.data = table;
                response.mgs = "Operación realizada con éxito";
                response.estatus = 1;
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error execture query: {ex.ToString()}");
                response.estatus = -1;
                response.mgs = ex.Message;
                response.data = null;

            }

            return response;
        }


        public ResponseObj Update(Func<Table<T>, T> predicate)
        {
            ResponseObj response = new ResponseObj();

            try
            {

                var table = TableGeneric<T>();

                var item = predicate(table);
                //this.Refresh(RefreshMode.KeepChanges, item);

                if (item == null)
                {
                    response.estatus = 0;
                    response.data = null;
                    response.mgs = "No fue posible realizar la operación";

                    return response;
                }


                this.SubmitChanges(ConflictMode.ContinueOnConflict);

                response.estatus = 1;
                response.data = item;
                response.mgs = "Operación realizada con éxito";

            }
            catch (Exception ex)
            {
                response.estatus = -1;
                response.data = null;
                response.mgs = ex.ToString();
            }

            return response;
        }


        public ResponseObj Insert(Func<Table<T>, T> predicate)
        {
            ResponseObj response = new ResponseObj();

            try
            {
                var table = TableGeneric<T>();

                var item = predicate(table);

                if (item == null)
                {
                    response.estatus = 0;
                    response.data = null;
                    response.mgs = "No fue posible realizar la operación";

                    return response;
                }

                table.InsertOnSubmit(item);

                this.SubmitChanges();

                response.estatus = 1;
                response.data = item;
                response.mgs = "Operación realizada con éxito";

            }
            catch (Exception ex)
            {
                response.estatus = -1;
                response.data = null;
                response.mgs = ex.Message;
            }

            return response;
        }


        public void ExecuteQuery(Action<Table<T>> predicate)
        {
            var obj = TableGeneric<T>();

            predicate?.Invoke(obj);

        }


        public List<T> Tolist2<T>(Func<T, bool> predicate) where T : class
        {
            var obj = TableGeneric<T>();
            T instance = Activator.CreateInstance<T>();
            var query = (from x in obj where predicate(instance) select x).ToList();

            return query;
        }

        public List<T> Tolist<T>(Func<Table<T>, IQueryable<T>> ts) where T : class
        {
            var obj = TableGeneric<T>();

            var x = ts(obj);

            var query = (x).ToList();

            return query;
        }


        public static string ConfigureConnection()
        {

            //var User = "sa";
            //var server = @"DESKTOP-8TICO3F\SQLEXPRESS";
            //var pwdPassword = "12345";
            //var database = "TEST";

            var User = "sa";
            var server = @"WIN-4M48VUCOEDO\UIN";
            var pwdPassword = "Dynsa2011";
            var database = "Campus_UIN";


            var connectionString = $@"Data Source={server};Initial Catalog={database};User ID={User};Password={pwdPassword};";

            return connectionString;
        }
    }
}
