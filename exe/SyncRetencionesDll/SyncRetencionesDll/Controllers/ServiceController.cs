﻿using SyncRetencionesDll.Models;
using System;
using System.Collections.Generic;
using System.Threading;

namespace SyncRetencionesDll.Controllers
{
    public class ServiceController
    {
        Thread Worker = null;
        public void Start()
        {
            ThreadStart start = new ThreadStart(() =>
            {
                while (true)
                {
                    //ConfigGenerales_Model config = null;

                    //var numElementosBloque = 0;
                    //var numElementoBloqueActual = 0;
                    //var responseConfiguraciones = new ConfigGeneralesController().SelectAll();

                    //if (responseConfiguraciones.estatus == 1)
                    //{
                    //    try
                    //    {
                    //        var lsConfiguraciones = responseConfiguraciones.data as List<ConfigGenerales_Model>;

                    //        config = lsConfiguraciones[0];
                    //        if (config.fgActivo)
                    //        {
                    //            numElementosBloque = config.numCantidadActualizacion;
                    //        }
                    //        else
                    //        {
                    //            Thread.Sleep(10000);
                    //            continue;
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        LogController.RegisterLog("getConfiguraciones", "cast obj", ex.ToString());
                    //        Thread.Sleep(10000);
                    //        continue;
                    //    }

                    //}
                    //else
                    //{
                    //    Thread.Sleep(10000);
                    //    continue;
                    //}

                    var configuracionesResponse = new SyncConfigController().SelectAll();

                    if (configuracionesResponse.estatus == 1)
                    {
                        var lsConfiguraciones = configuracionesResponse.data as List<SyncConfig_Model>;

                        foreach (var config_Item in lsConfiguraciones)
                        {
                            if (!Convert.ToBoolean(config_Item.FlagSync)) continue;

                            //var values = new BITACORA_ALUMNOS_INSERT_Controller().db.ExecuteQueryStringGet("select * from zAxt_ProcesosCOB");


                            //Ejemplo de insert para bitacora
                            //var responseBitacoraInsert = new BITACORA_ALUMNOS_INSERT_Controller().Insert(new BITACORA_ALUMNOS_INSERT_Model());


                            if (config_Item.TpConfig == "Alumnos")
                            {

                                var ctrl = new AlumnosController();

                                //Validaciones para el insert de alumnos
                                ctrl.InsertAlumnos(config_Item.CountRegister);

                            }
                            else if (config_Item.TpConfig == "Retenciones")
                            {

                            }
                            else
                            {
                                continue;
                            }

                            //Calcular el tiempo de descanso                    
                            var time = (Convert.ToBoolean(config_Item.FlagSeconds) ? config_Item.Interval : (config_Item.Interval * 60)) * 1000;
                            Thread.Sleep(time);
                        }
                    }
                    else
                    {
                        LogController.RegisterLog("AlumnosController:InsertAlumnos", "InsertAlumnos->Bit", $"estatus: {configuracionesResponse.estatus}- Mensaje {configuracionesResponse.mgs}");
                        //Terminar el proceso
                        Thread.Sleep(15000);
                    }

                    //    var response = new zAxtProcesosCOBController().SelectAll();

                    //    //var response2 = new zAxtProcesosNDBController().SelectAll();

                    //    //var ctrl = new zAxtProcesosNDBController();

                    //    //ctrl.db.ExecuteQuery(table =>
                    //    //{
                    //    //    List<zAxt_ProcesosNDB_Model> result = (from a in table where a.numCobro == "COB000000594" select a).ToList();

                    //    //});

                    //    while (numElementoBloqueActual <= numElementosBloque)
                    //    {
                    //        //Procesar elementos aqui

                    //        //Validar aqui como debera ir esas validaciones

                    //        numElementoBloqueActual++;
                    //    }

                    //    goto Finish;
                    //Finish:
                    //Siguiente vuelta
                    //Thread.Sleep(15000);
                }
            });

            Worker = new Thread(start);
            Worker.Start();
        }


        public void Stop()
        {
            try
            {
                if ((Worker != null) && Worker.IsAlive)
                {
                    Worker.Abort();
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
