﻿using System;
using System.IO;

namespace SyncRetencionesDll.Controllers
{
    public class LogController
    {
        public static LogController GetInstance() => new LogController();
        public static void RegisterLog(string function, string nivelCatch, string message)
        {
            //Extensions.RegisterLog()
            var fileName = $"{Constants.path_Log}Log_{DateTime.Now.ToString("ddMMyyyy")}.txt";

            try
            {
                if (!Directory.Exists(Constants.path_Log))
                    Directory.CreateDirectory(Constants.path_Log);

                using (StreamWriter writer = new StreamWriter(fileName, true))
                {
                    writer.WriteLine($"--------------------------Start-----------------------------");

                    writer.WriteLine($"function: {function}");
                    writer.WriteLine($"nivelCatch: {nivelCatch}");
                    writer.WriteLine($"Hora: {DateTime.Now.ToString("hh:mm:ss tt")}");
                    writer.WriteLine($"message: {message}");

                    writer.WriteLine($"---------------------------Finish----------------------------\n\n");
                    writer.Close();
                }
            }
            catch (Exception ex)
            { //Error write in log }
            }
        }
    }
}
