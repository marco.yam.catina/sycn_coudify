﻿using SyncRetencionesDll.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
using TestDao.Models;

namespace TestDao.Controllers
{
    public class BaseController<T> where T : class
    {
        public DaoContext<T> db = new DaoContext<T>();

        public string GetValuesModel(T item, T currenItem)
        {
            StringBuilder str = new StringBuilder();

            try
            {
                Type type = typeof(T);
                var properties = type.GetProperties();

                var isFirst = true;

                foreach (var pro in properties)
                {
                    var value = pro.GetValue(item);
                    var valueOrigin = pro.GetValue(currenItem);



                    if (value == null) continue;

                    if (value is Guid) continue;

                  
                    //LogController.RegisterLog("Compare values", "values", $"Value txt [{value.ToString()}]- value db [{valueOrigin.ToString()}]");

                    //Solo validar que los datos ingresados sean diferentes
                    if (value.ToString() == valueOrigin.ToString()) continue;

                    var realValue = $"[{ pro.Name}] = ";

                    //ParaDirecciones
                    if (realValue.Contains("addr1") || realValue.Contains("addr2") || realValue.Contains("addr3") || realValue.Contains("addr4"))
                        realValue = realValue.Replace("addr", "addr##");

                    if (value is string)
                    {
                        realValue += $"N'{value.ToString()}'";
                    }
                    else if (value is DateTime)
                    {
                        var date_Value = (DateTime.Parse(value.ToString()));

                        realValue += $"'{date_Value.ToString("yyyy-MM-dd HH:mm:sss")}'";
                    }
                    else if (value is bool)
                    {
                        realValue += $"{(((bool)value) == true ? 1 : 0)}";
                    }
                    else
                    {
                        realValue += $"{value.ToString()}";
                    }

                    var valueSend = (!isFirst ? "," : "") + realValue;

                    isFirst = false;

                    str.AppendLine(valueSend);
                }
            }
            catch (Exception ex)
            {
                str.Clear();
            }

            return str.ToString();
        }
        public ResponseObj SelectAll()
        {
            ResponseObj response = new ResponseObj();

            try
            {
                response = db.SelectAll();
            }
            catch (Exception ex)
            {
                response.estatus = -1;
                response.data = null;
                response.mgs = ex.ToString();
            }

            return response;
        }


    }
}
