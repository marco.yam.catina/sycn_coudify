﻿using SyncRetencionesDll.Models;
using TestDao.Controllers;

namespace SyncRetencionesDll.Controllers
{
    public class GenericController
    {

    }

    public class DEMOGRAPHICS_ModelController : BaseController<DEMOGRAPHICS_Model>
    {

    }

    public class ADDRESSSCHEDULE_ModelController : BaseController<ADDRESSSCHEDULE_Model>
    {

    }

    public class USERDEFINEDIND_ModelController : BaseController<USERDEFINEDIND_Model>
    {

    }
    public class PersonPhone_ModelController : BaseController<PersonPhone_Model>
    {

    }
}
