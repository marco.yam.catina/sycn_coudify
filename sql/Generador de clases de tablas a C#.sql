
DECLARE @TABLE_NAME NVARCHAR(200) = 'BITACORA_ALUMNOS_INSERT'

DECLARE @COLUMN_NAME NVARCHAR(MAX), @DATA_TYPE NVARCHAR(MAX)


declare @table_code table(valuex nvarchar(max))

insert into @table_code values('[Table(Name = "'+@TABLE_NAME+'")]')
insert into @table_code values('public class '+@TABLE_NAME+'_Model')
insert into @table_code values('{')

			
DECLARE CURSOR_COLUMN CURSOR FOR

SELECT 
COLUMN_NAME,
DATA_TYPE
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @TABLE_NAME

OPEN CURSOR_COLUMN --INICIAMOS EL CURSOR
	
FETCH NEXT FROM CURSOR_COLUMN INTO @COLUMN_NAME,@DATA_TYPE
		
WHILE (@@FETCH_STATUS = 0)
BEGIN
	DECLARE @LINE_CODE NVARCHAR(MAX) = ''

	insert into @table_code values('')
	SET @LINE_CODE += 'public '

	--CODIGO FUNCIONAL SOLO PARA RICH
	BEGIN
			SELECT 
			   @DATA_TYPE = DATA_TYPE
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME=@TABLE_NAME AND COLUMN_NAME = @COLUMN_NAME
	END
	SET @LINE_CODE += CASE LOWER(@DATA_TYPE)
						WHEN 'numeric' THEN 'decimal'
						WHEN 'decimal' THEN 'decimal'
						WHEN 'int' THEN 'int'
						WHEN 'smallint' THEN 'int'
						WHEN 'tinyint' THEN 'int'
						WHEN 'char' THEN 'string'
						WHEN 'nchar' THEN 'string'
						WHEN 'nvarchar' THEN 'string'
						WHEN 'nvarchar(max)' THEN 'string'
						WHEN 'datetime' THEN 'DateTime'
						WHEN 'time' THEN 'TimeSpan'
						WHEN 'bit' THEN 'bool'
						WHEN 'float' THEN 'double'
						when 'uniqueidentifier' then 'string'
						WHEN 'varbinary' THEN 'byte[]'
						ELSE 'NO_DATA_TYPE'
						END

	SET @LINE_CODE += ' '+@COLUMN_NAME
	SET @LINE_CODE += ' {get;set;}'

	DECLARE @tagname nvarchar(500)= '[Column(Name = "'+@COLUMN_NAME+'")]'

	insert into @table_code values(@tagname)

	insert into @table_code values(@LINE_CODE)

	FETCH NEXT FROM CURSOR_COLUMN INTO @COLUMN_NAME,@DATA_TYPE
END
close CURSOR_COLUMN
deallocate CURSOR_COLUMN

insert into @table_code values('}')


select * from @table_code